import torch
import torch.nn as nnet
from transformer_block import TransformerBlock
import numpy as np 


def Embedding(num_embeddings, embedding_dim, padding_idx=None):
    m = nnet.Embedding(num_embeddings, embedding_dim, padding_idx=padding_idx)
    nnet.init.normal_(m.weight, mean=0, std=embedding_dim ** -0.5)
    if padding_idx is not None:
        nnet.init.constant_(m.weight[padding_idx], 0)
    return m


class Encoder(nnet.Module):

    # Constructors
    def __init__(self, device, src_vocabular_size, embed_size, num_heads, num_layers, forward_expansion, dropout, max_lenght):

        super(Encoder, self).__init__();
        
        self.device = device;
        
        # Embeddings
        self.embeddings = Embedding(src_vocabular_size, embed_size);
        self.position_encoding(embed_size, max_lenght);
        
        self.layers = nnet.ModuleList([TransformerBlock(embed_size,num_heads,forward_expansion,dropout) for i in range(num_layers)]);
        
        
    def position_encoding(self, embed_size, max_lenght):
        
        self.position_embedding =  Embedding(max_lenght, embed_size);

        # Sinusoidal weights
        position_enc = np.array([[pos / np.power(10000, 2 * (j // 2) / embed_size) for j in range(embed_size)]for pos in range(max_lenght)]);
        self.position_embedding.weight[:, 0::2] = torch.FloatTensor(np.sin(position_enc[:, 0::2]));
        self.position_embedding.weight[:, 1::2] = torch.FloatTensor(np.cos(position_enc[:, 1::2]));
        self.position_embedding.weight.detach_();
        self.position_embedding.weight.requires_grad = False;
        
    def forward(self, x, mask):
        
        # Embeddings
        positions = torch.arange(x.size(1), out=positions, device=self.device).expand(x.size(0),x.size(1));
        x = self.dropout(self.embeddings(x) + self.position_embedding(positions));
        
        # Positional embeddings
        seq_len, dim_model = x.size(1), x.size(2);
        
        x += self.position_encoding(seq_len, dim_model);
        
        # Apply encoder layers
        for layer in self.layers:
            x = layer(x, x, x, mask);
        
        return x;
    
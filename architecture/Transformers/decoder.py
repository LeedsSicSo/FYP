import torch
import torch.nn as nnet
from decoder_block import DecoderBlock 
import numpy as np

def Embedding(num_embeddings, embedding_dim, padding_idx=None):
    m = nnet.Embedding(num_embeddings, embedding_dim, padding_idx=padding_idx)
    nnet.init.normal_(m.weight, mean=0, std=embedding_dim ** -0.5)
    if padding_idx is not None:
        nnet.init.constant_(m.weight[padding_idx], 0)
    return m

class Decoder(nnet.Module):

    # Constructors
    def __init__(self, device, trg_vocab_size, embed_size, num_heads, num_layers, forward_expansion, dropout, max_lenght):

        super(Decoder, self).__init__();
        
        self.device = device;
        
        # Embeddings
        self.embeddings = Embedding(trg_vocab_size, embed_size);
        self.position_encoding(embed_size, max_lenght);
        
        # Decoder layers
        self.layers = nnet.ModuleList([DecoderBlock(embed_size, num_heads, forward_expansion, dropout) for i in range(num_layers)]);
        
        # Linear layer
        self.out = nnet.Linear(embed_size, trg_vocab_size);
        
        # Dropout layer
        self.dropout = nnet.Dropout(dropout);
        
    
    def position_encoding(self, embed_size, max_lenght):
        
        self.position_embedding =  Embedding(max_lenght, embed_size);

        position_enc = np.array([[pos / np.power(10000, 2 * (j // 2) / embed_size) for j in range(embed_size)]for pos in range(max_lenght)]);
        
        self.position_embedding.weight[:, 0::2] = torch.FloatTensor(np.sin(position_enc[:, 0::2]));
        self.position_embedding.weight[:, 1::2] = torch.FloatTensor(np.cos(position_enc[:, 1::2]));
        self.position_embedding.weight.detach_();
        self.position_embedding.weight.requires_grad = False;
        
    def forward(self, x, enc_out, src_mask, trg_mask):
        
        # Embeddings
        positions = torch.arange(x.size(1), out=positions, device=self.device).expand(x.size(0),x.size(1));
        x = self.dropout(self.embeddings(x) + self.position_embedding(positions));

        # Apply decoder layers
        for layer in self.layers:
            x = layer(x, enc_out, enc_out, src_mask, trg_mask);
            
        # Apply linear layer
        out = self.out(x);
        
        return out;


    
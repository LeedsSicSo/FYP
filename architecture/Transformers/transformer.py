import torch
import torch.nn as nnet
from encoder import Encoder 
from decoder import Decoder 

class Transformer(nnet.Module):

    # Constructors
    def __init__(self, device, src_vocabular_size, src_pad, trg_pad, trg_vocabular_size, embed_size, num_heads, num_layers_encoder, num_layers_decoder, forward_expansion, dropout, max_length):

        super(Transformer, self).__init__();
        
        # Device
        self.device = device;
        
        self.src_pad = src_pad;
        self.trg_pad_idx = trg_pad;
        
        # Encoder blocks
        self.encoders = Encoder(device, src_vocabular_size, embed_size, num_heads, num_layers_encoder, forward_expansion, dropout, max_length);
        
        # Decodr blocks
        self.decoders = Decoder(device, trg_vocabular_size, embed_size, num_heads, num_layers_decoder, forward_expansion, dropout, max_length);
        
    def make_src_mask(self, src):
        
        src_mask = (src != self.src_pad).unsqueeze(1).unsqueeze(2)

        return src_mask.to(self.device)

    def make_trg_mask(self, trg):
        
        training_values, trg_len = trg.shape
        
        trg_mask = torch.tril(torch.ones((trg_len, trg_len))).expand(training_values, 1, trg_len, trg_len)

        return trg_mask.to(self.device)

    def forward(self, src, trg):
        
        src_mask = self.make_src_mask(src);
        
        trg_mask = self.make_trg_mask(trg);
        
        enc_src = self.encoders(src, src_mask);
        
        out = self.decoders(trg, enc_src, src_mask, trg_mask);
        
        return out;
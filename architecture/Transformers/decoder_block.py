import torch
import torch.nn as nnet
from transformer_block import TransformerBlock 
from attention import Attention

class DecoderBlock(nnet.Module):

    # Constructors
    def __init__(self, embed_size, num_heads, forward_expansion, dropout):

        super(DecoderBlock, self).__init__();
        # Maked attention layer
        self.attention_masked = Attention(embed_size, num_heads);
        
        # Normalization after masked attention
        self.norm = nnet.LayerNorm(embed_size);
        
        # Dropout layer
        self.dropout = nnet.Dropout(dropout);
        
        # Transfoermer layer
        self.transformer_block = TransformerBlock(embed_size, num_heads, forward_expansion, dropout);


    def forward(self, x, value, key, trg_mask, src_mask=None):
        # Maked attention layer
        attention = self.attention_masked(x, x, x, trg_mask);
        # Addition + Normalisation (creates the queries for transforners block)
        query = self.dropout(self.norm(attention + x));
        # Transformer layer
        out = self.transformer_block(value, key, query, src_mask);
        
        return out;

import torch
import torch.nn as nnet

MIN_VALUE = -0.0000000001

class Attention(nnet.Module):

    # Constructors
    def __init__(self, embed_size, heads):

        super(Attention, self).__init__();

        # Nunber of inputs (words)
        self.embed_size = embed_size;
        # Number of heads
        self.heads = heads;

        # Head dimsnesion
        self.head_dimension = 0;
        self.compute_head_dim();

        # Create used structures
        self.create_structures();

    def compute_head_dim(self):
        
        # Heads need to divide embed sizes
        assert(self.embed_size % self.heads == 0);

        self.head_dimension = int(self.embed_size / self.heads);

    def create_structures(self):

        self.values = nnet.Linear(self.head_dimension, self.head_dimension, bias = False);
        self.keys = nnet.Linear(self.head_dimension, self.head_dimension, bias = False);
        self.queries = nnet.Linear(self.head_dimension, self.head_dimension, bias = False);

        self.out = nnet.Linear(self.embed_size, self.embed_size);


    def forward(self, values, keys, queries, mask = None):

        # Shape : training examples * number of words
        training_values = queries.shape[0];
        v_len = values.shape[1];
        k_len = keys.shape[1];
        q_len = queries.shape[1];

        values = values.reshape(training_values, v_len, self.heads, self.head_dimension);
        keys = keys.reshape(training_values, k_len, self.heads, self.head_dimension);
        queries = queries.reshape(training_values, q_len, self.heads, self.head_dimension);

        aux = torch.einsum("tqhd,tkhd->thqk", [queries, keys]);

        if mask is not None:
            aux = aux.masked_fill(mask = 0, value = MIN_VALUE);

        attention = torch.softmax(aux / (self.embed_size**(1/2)), dim = 3);

        out = torch.einsum("thqk,tvhd->tqhd", [attention, values]);

        out = out.reshape(training_values, q_len, self.embed_size);

        return self.out(out);

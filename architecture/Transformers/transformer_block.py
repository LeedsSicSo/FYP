import torch.nn as nnet
from attention import Attention 

class TransformerBlock(nnet.Module):

    # Constructors
    def __init__(self, embed_size, heads, forward_expansion, dropout, gelu = True):

        super(TransformerBlock, self).__init__();

        # Attention layer
        self.attention = Attention(embed_size, heads);

        # Normalization after attention
        self.norm1 = nnet.LayerNorm(embed_size);
        # Normalization after network
        self.norm2 = nnet.LayerNorm(embed_size);

        # Feed forward network layer
        if gelu:
            self.network = nnet.Sequential(nnet.Linear(embed_size, forward_expansion * embed_size), nnet.GELU(), nnet.Linear(forward_expansion * embed_size,embed_size));
        else:
            self.network = nnet.Sequential(nnet.Linear(embed_size, forward_expansion * embed_size), nnet.ReLU(), nnet.Linear(forward_expansion * embed_size,embed_size));

        # Dropout layer
        self.dropout = nnet.Dropout(dropout);

    def forward(self, values, keys, queries, mask = None):
        # Attention layer   
        attention = self.attention(values, keys, queries, mask);
        # Addition + Normalisation 
        res1 = self.dropout(self.norm1(attention + queries));
        # Feed forward layer
        network = self.network(res1);
        # Addition + Normalisation 
        res2 = self.dropout(self.norm2(network + res1))

        return res2;

        




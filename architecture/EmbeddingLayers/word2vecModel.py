import torch.nn as nn

class CBOW_Model(nn.Module):
    def __init__(self, vocab_size, embed_dim, embed_max_norm):
        super(CBOW_Model, self).__init__()
        self.embeddings = nn.Embedding(
            num_embeddings=vocab_size,
            embedding_dim=embed_dim,
            max_norm=embed_max_norm,
        );
        self.linear = nn.Linear(
            in_features=embed_dim,
            out_features=vocab_size,
        );

    def forward(self, inputs_):
        x = self.embeddings(inputs_);
        x = x.mean(axis=1);
        x = self.linear(x);
        return x;
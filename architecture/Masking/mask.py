import torch

def contains_item(item, list):
    for i in list:
        if i == item:
            return True;
    return False;

class Mask:
    # Constructors
    def __init__(self, mask_id):
        # Mask id
        self.mask_id = mask_id;
        
    
    def mask_array(self, x, percentage, special_tokens = []):
        rand_array = torch.rand(x.shape);
        mask_array = (rand_array < percentage / 100.0) * (not contains_item(x,special_tokens));
        return mask_array;
    
    def apply_mask(self, x, mask_array, copy=False):
        mask_index = torch.flatten(mask_array[0].nonzero()).tolist();
        if copy:
            out  = ;
            return out;
        else:
            x[mask_index] = self.mask_id;
    
    
        
        
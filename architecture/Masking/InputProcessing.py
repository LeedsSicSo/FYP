import numpy as np
import torch
from mask import Mask


class Processing:

    def __init__(self, special_characters):
        self.special_characters = special_characters;

    def drop_words(self, x, lengths, dropout_percent):

        assert 0 < dropout_percent < 100

        # define words to drop
        eos = self.params.eos_index
        assert (x[0] == eos).sum() == lengths.size(0)
        keep = np.random.rand(x.size(0) - 1, x.size(1)) >= dropout_percent/100

        keep[0] = 1  # do not drop the start sentence symbol

        sentences = []
        aux_lengths = []
        for i in range(lengths.size(0)):
            assert x[lengths[i] - 1, i] == eos
            words = x[:lengths[i] - 1, i].tolist()
            # randomly drop words from the input
            new_s = [w for j, w in enumerate(words) if keep[j, i]]
            # we need to have at least one word in the sentence (more than the start / end sentence symbols)
            if len(new_s) == 1:
                new_s.append(words[np.random.randint(1, len(words))])
            new_s.append(eos)
            assert len(new_s) >= 3 and new_s[0] == eos and new_s[-1] == eos
            sentences.append(new_s)
            aux_lengths.append(len(new_s))
        # re-construct input
        l2 = torch.LongTensor(aux_lengths)
        x2 = torch.LongTensor(l2.max(), l2.size(
            0)).fill_(self.params.pad_index)
        for i in range(l2.size(0)):
            x2[:l2[i], i].copy_(torch.LongTensor(sentences[i]))
        return x2, l2

    def shuffle_words(self, x, lengths, max_distr):

        # define noise word scores
        noise = np.random.uniform(0, max_distr, size=(x.size(0) - 1, x.size(1)))
        noise[0] = -1

        assert max_distr > 1

        x2 = x.clone()
        for i in range(lengths.size(0)):
            # generate a random permutation
            scores = np.arange(lengths[i] - 1) + noise[:lengths[i] - 1, i]
            permutation = scores.argsort()
            # shuffle words
            x2[:lengths[i] - 1, i].copy_(x2[:lengths[i] - 1, i][torch.from_numpy(permutation)])
        return x2, lengths

    def mask_words(self, x, lengths, percentage):
        mask = Mask();
        mask.apply_mask(x,mask.mask_array(x,percentage,self.special_characters));
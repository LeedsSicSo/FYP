﻿using System.IO;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;

namespace CreateFilesFromCSV
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.Tracing;
    using System.Threading;
    using System.Threading.Tasks;
    using Sylvan.Data.Csv;
    using static CreateFilesFromCSV.FileClass;


    public class CSVProcess
    {
        //Class variables
        private readonly object _balanceLock;
        private readonly object _balanceLock2;
        private long _counterFiles;
        private readonly string counter_path;
        private readonly string loc_path;

        //Constructor
        public CSVProcess(string counter_path, string loc_path)
        {
            _counterFiles = 0;
            _balanceLock = new object();
            _balanceLock2 = new object();
            this.counter_path = counter_path;
            this.loc_path = loc_path;
        }

        //Sequential testing
        public void print_row(string path_file)
        {
            var options = new CsvDataReaderOptions
            {
                BufferSize = 0x1000000,
            };
            var csv = CsvDataReader.Create(path_file, options);
            var counter = 0;

            while (csv.Read())
            {

                var id = csv.GetString(0);
                var path = csv.GetString(1);

                //FileClass fc = new FileClass(id,path);

                //var content = csv.GetString(2);
                //Console.WriteLine(id + " " + path + " " + content);
                //Console.WriteLine(id);
                Console.WriteLine(path);
                //Console.WriteLine(fc.Path);

                counter++;
                if (counter >= 100)
                    break;
            }
        }
        //Async operations
        private async Task<bool> create_file_from_row(CsvDataReader csv,string counter_path,string location_path="")
        {

            if (! csv.Read())
                return false;

            
            var id = csv.GetString(0);
            var path = csv.GetString(1);

            FileClass fc = new FileClass(id, path);

            //Create file
            var content = csv.GetString(2);
            //fc.write_string(content, location_path);

            //Console.WriteLine(fc.Path);
            lock (_balanceLock)
            {
                _counterFiles++;
                File.WriteAllText(counter_path, _counterFiles.ToString());
                //Console.WriteLine(_counterFiles);
            }

            return true;
        }

        public async Task print_row_async(string path_file)
        {

            var options = new CsvDataReaderOptions
            {
                BufferSize = 0x1000000,
            };

            await using var csv = await CsvDataReader.CreateAsync(path_file, options);

           List<Task<bool>> jobs =  new List<Task<bool>>();
           bool val = true;

           while (val)
           {
               val = await create_file_from_row(csv, counter_path, loc_path);
               //jobs.Add(create_file_from_row(csv, counter_path, loc_path));
              // Task<bool> finishedTask = await Task.WhenAny(jobs);
               //jobs.Remove(finishedTask);
               //val = await finishedTask;
           }

           //Task.WaitAll(jobs.ToArray());

           //Console.WriteLine(_counterFiles.ToString());
        }
    }
}
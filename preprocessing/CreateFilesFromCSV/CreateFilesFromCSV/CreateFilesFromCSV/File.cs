﻿using Microsoft.CSharp.RuntimeBinder;
using Sylvan.Data.Csv;

namespace CreateFilesFromCSV
{
    using System.IO;


    enum FileType : int
    {
        Cs=1,Py=2,Cpp=3,Java=4,Unk=0
    }

    public class FileClass
    {
        //Class variables
        public string Path { get; }
        private FileType _type = FileType.Unk;

        //Constructors
        public FileClass(string id, string prev_path)
        {
            Path = create_path(id, prev_path);
        }

        //Create path file
        private string create_path(string id, string pre_path)
        {
            //Create new path structure based on file type
            _type = get_type(pre_path);
            int char_ext = num_char_ext();
            string folder_name_sub = folder_name();
            string ext = pre_path.Substring(pre_path.Length-char_ext);
            string proc_pre_path = pre_path.Substring(0,pre_path.Length-char_ext);

            //Eliminate previous folder structure
            int index = proc_pre_path.LastIndexOf('/');

            //Assemble path
            if (index == -1)
                return folder_name_sub + "\\" + proc_pre_path + "." + id + ext;
            return folder_name_sub + "\\" + proc_pre_path.Substring(index+1) + "." + id + ext;
        }
        
        public bool exists()
        {
            return File.Exists(Path);
        }

        public bool write_string(string text, string location="")
        {
            if (exists()) 
                return false;
            // Create a file to write to.
            using StreamWriter sw = File.CreateText(location + Path);
            sw.Write(text);
            return true;
        }

        //File type functions
        private FileType get_type(string path)
        {
            if (path.EndsWith(".cs"))
                return FileType.Cs;
            if (path.EndsWith(".pt"))
                return FileType.Py;
            if (path.EndsWith(".cpp"))
                return FileType.Cpp;
            if (path.EndsWith(".java"))
                return FileType.Java;
            return FileType.Unk;
        }
        private int num_char_ext()
        {
            return _type switch
            {
                FileType.Cs => 3,
                FileType.Py => 3,
                FileType.Cpp => 4,
                FileType.Java => 5,
                _ => 0
            };
        }

        private string folder_name()
        {
            return _type switch
            {
                FileType.Cs => "csharp",
                FileType.Py => "python",
                FileType.Cpp => "cplusplus",
                FileType.Java => "java",
                _ => "unknown"
            };
        }
    }
}
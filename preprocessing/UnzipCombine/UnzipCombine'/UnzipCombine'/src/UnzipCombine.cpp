// UnzipCombine'.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif


#include <iostream>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <vector>
#include <zlib/zlib.h>
#include <filesystem>
#include <fstream>

#if defined(MSDOS) || defined(OS2) || defined(WIN32) || defined(__CYGWIN__)
#  include <fcntl.h>
#  include <io.h>
#  define SET_BINARY_MODE(file) setmode(fileno(file), O_BINARY)
#else
#  define SET_BINARY_MODE(file)
#endif


#define CHUNK 16384


#define Uint unsigned int 

typedef struct batchStatus
{
    bool read;
    long int bytes_in_batch;
};


std::string read_line(gzFile file)
{
    std::vector< char > v(256);
    unsigned pos = 0;
    for (;; ) {
        if (gzgets(file, &v[pos], v.size() - pos) == 0) {
            // end-of-file or error
            int err;
            const char* msg = gzerror(file, &err);
            if (err != Z_OK) {
                // handle error
            }
            break;
        }
        unsigned read = strlen(&v[pos]);
        if (v[pos + read - 1] == '\n') {
            if (pos + read >= 2 && v[pos + read - 2] == '\r') {
                pos = pos + read - 2;
            }
            else {
                pos = pos + read - 1;
            }
            break;
        }
        if (read == 0 || pos + read < v.size() - 1) {
            pos = read + pos;
            break;
        }
        pos = v.size() - 1;
        v.resize(v.size() * 2);
    }
    v.resize(pos);
    return std::string(v.begin(), v.end());
}


batchStatus read_bytes(gzFile file, unsigned char* buffer, Uint num_bytes)
{
    const int num_bytes_read = gzread(file, buffer, num_bytes-1);
    buffer[num_bytes_read] = '\0';
    if (num_bytes_read+1 < num_bytes)
        return batchStatus{ false,num_bytes_read };
    return batchStatus{ true, num_bytes_read };
}
void write_block(FILE* file, unsigned char* buffer, Uint num_bytes)
{
    fwrite(buffer, sizeof(char), num_bytes, file);
}

std::vector<std::string>getFileList(const std::string& path)
{
    std::vector<std::string> res;
    for (const auto& file : std::filesystem::directory_iterator(path))
        res.push_back(file.path().string());
    return res;
}

std::ofstream record_file;

void completed_file(std::string file_name)
{
    record_file << file_name << std::endl;
}

void process_file(const std::string& input_file_path, FILE* output_file, bool include_header, long int bytes)
{
    gzFile input_file = gzopen(input_file_path.c_str(), "r");
    if (!include_header)
        read_line(input_file);
    unsigned char* buffer = static_cast<unsigned char*>(malloc(bytes * sizeof(unsigned char)));
    batchStatus status{ true, 0 };
    while(status.read)
    {
        status = read_bytes(input_file, buffer, bytes);
        write_block(output_file,buffer, status.bytes_in_batch);
    }
    free(buffer);
    completed_file(input_file_path);
}

//Testing
void print_files(const std::vector<std::string>list)
{
    for (Uint i = 0; i < list.size(); i++)
        std::cout << list[i] << std::endl;
}

std::string input_path = "H:/Repos/final_table/final_table1/final_combined";
std::string output_path = "H:/Repos/final_table/final_table1/full_file/final.csv";
std::string record_path = "C:/Users/SicSo/Desktop/PreprocessingProgress/finished_files.txt";

int main()
{
	const std::vector<std::string>list =  getFileList(input_path);
    
    FILE* output_file = fopen(output_path.c_str(), "ab");
    record_file.open(record_path);

    //256Mb - size of hard disk cache(to improve performance by decreasing latency)
    const long int bytes = 256000000;
    //100Mb
    //const long int bytes = 100000000;

    process_file(list[0], output_file,true,bytes);
    for (Uint i = 1; i < list.size(); i++)
        process_file(list[i], output_file,false,bytes);

}
import argparse
import logging
import os
import random
import re
import sys
from io import BytesIO

import clang
clang.cindex.Config.set_library_file('C:/Computer_Science/RandomSoft/LLVM/bin/libclang.dll')

from Lexers import JavaLexer as jlex

class JAVATokeniser(Tokeniser):

    JAVA_TOKEN2CHAR = {'STOKEN0': "//",
                    'STOKEN1': "/*",
                    'STOKEN2': "*/",
                    'STOKEN3': "/**",
                    'STOKEN4': "**/",
                    'STOKEN5': '"""',
                    'STOKEN6': '\\n'
                    }
    JAVA_CHAR2TOKEN = {"//": ' STOKEN0 ',
                    "/*": ' STOKEN1 ',
                    "*/": ' STOKEN2 ',
                    "/**": ' STOKEN3 ',
                    "**/": ' STOKEN4 ',
                    '"""': ' STOKEN5 ',
                    '\\n': ' STOKEN6 '
                    }

    def tokenize_java(self, s, keep_comments=False):
        try:
            tokens = []
            assert isinstance(s, str)
            s = s.replace(r'\r', '')
            tokens_generator = jlex.tokenize(s, keep_comments=keep_comments)
            for token in tokens_generator:
                if isinstance(token, jlex.String):
                    tokens.append(Tokeniser.process_string(token.value, self.JAVA_CHAR2TOKEN, self.JAVA_TOKEN2CHAR, False))
                elif isinstance(token, jlex.Comment):
                    com = Tokeniser.process_string(token.value, self.JAVA_CHAR2TOKEN, self.JAVA_TOKEN2CHAR, True)
                    if len(com) > 0:
                        tokens.append(com)
                else:
                    tokens.append(token.value)
            return tokens
        except:
            return []


    def detokenize_java(s):
        assert isinstance(s, str) or isinstance(s, list)
        if isinstance(s, list):
            s = ' '.join(s)
        s = s.replace('ENDCOM', 'NEW_LINE')
        s = s.replace('▁', 'SPACETOKEN')

        s = s.replace('} "', 'CB_ "')
        s = s.replace('" {', '" OB_')
        s = s.replace('*/ ', '*/ NEW_LINE')
        s = s.replace('} ;', 'CB_COLON NEW_LINE')
        s = s.replace('} ,', 'CB_COMA')
        s = s.replace('}', 'CB_ NEW_LINE')
        s = s.replace('{', 'OB_ NEW_LINE')
        s = s.replace(';', '; NEW_LINE')
        lines = re.split('NEW_LINE', s)

        untok_s = indent_lines(lines)
        untok_s = untok_s.replace('CB_COLON', '};').replace(
            'CB_COMA', '},').replace('CB_', '}').replace('OB_', '{')
        untok_s = untok_s.replace('> > >', '>>>').replace('<< <', '<<<')
        untok_s = untok_s.replace('> >', '>>').replace('< <', '<<')

        try:
            # call parser of the tokenizer to find comments and string and detokenize them correctly
            tokens_generator = jlex.tokenize(untok_s, keep_comments=True)
            for token in tokens_generator:
                if isinstance(token, jlex.String) or isinstance(token, jlex.Comment):
                    token_ = token.value.replace('STRNEWLINE', '\n').replace('TABSYMBOL', '\t').replace(' ', '').replace(
                        'SPACETOKEN', ' ')
                    untok_s = untok_s.replace(token.value, token_)
        except KeyboardInterrupt:
            raise
        except:
            pass
        return untok_s


with open('Testing.java', 'r') as fin:
    tokens = fin.read()
    h=tokenize_java(tokens)
    fout = open("output_java_testing.txt", "w",encoding='utf-8')
    for token in h:
        fout.write(token + "\n")


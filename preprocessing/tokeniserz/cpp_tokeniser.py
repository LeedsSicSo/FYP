import argparse
import logging
import os
import random
import re
import sys
import tokenize
from io import BytesIO

import clang
from clang.cindex import TokenKind
clang.cindex.Config.set_library_file('C:/Computer_Science/RandomSoft/LLVM/bin/libclang.dll')

from tokenise_class import Tokeniser

#from sacrebleu import tokenize_v14_international


class CPPTokeniser(Tokeniser):

    idx = clang.cindex.Index.create();

    CPP_TOKEN2CHAR = {'STOKEN0': "//",
                    'STOKEN1': "/*",
                    'STOKEN2': "*/",
                    'STOKEN3': "/**",
                    'STOKEN4': "**/",
                    'STOKEN5': '"""',
                    'STOKEN6': '\\n'
                    }
    CPP_CHAR2TOKEN = {"//": ' STOKEN0 ',
                    "/*": ' STOKEN1 ',
                    "*/": ' STOKEN2 ',
                    "/**": ' STOKEN3 ',
                    "**/": ' STOKEN4 ',
                    '"""': ' STOKEN5 ',
                    '\\n': ' STOKEN6 '
                    }

    def get_cpp_tokens_and_types(self, s):
        tokens = []
        assert isinstance(s, str)
        s = s.replace(r'\r', '')
        hash = str(random.getrandbits(128))
        parsed_code = self.idx.parse(hash + '_tmp.cpp', args=['-std=c++11'], unsaved_files=[(hash + '_tmp.cpp', s)], options=0)
        for tok in parsed_code.get_tokens(extent=parsed_code.cursor.extent):
            tokens.append((tok.spelling, tok.kind))
        return tokens

    def tokenize_cpp(self, s, keep_comments=False):
        tokens = []
        assert isinstance(s, str)
        try:
            tokens_and_types = get_cpp_tokens_and_types(s)
            for tok, typ in tokens_and_types:
                if not keep_comments and typ == Tokeniser.TokenKind.COMMENT:
                    continue
                if typ in STRINGS_AND_COMMENTS_TOKEN_KINDS:
                    if typ == Tokeniser.TokenKind.COMMENT:
                        com = Tokeniser.process_string(tok, self.CPP_CHAR2TOKEN, self.CPP_TOKEN2CHAR, True)
                        if len(com) > 0:
                            tokens.append(com)
                    else:
                        tokens.append(Tokeniser.process_string(tok, self.CPP_CHAR2TOKEN, self.CPP_TOKEN2CHAR, False))
                else:
                    tokens.append(tok)
            return tokens
        except KeyboardInterrupt:
            raise
        except TimeoutError:
            print(f'TimeOut Error')
            logging.info('*' * 20)
            logging.info(f'TimeOut Error for string {s}')
            return []
        except:
            return []

    def detokenize_cpp(s):
        assert isinstance(s, str) or isinstance(s, list)
        if isinstance(s, list):
            s = ' '.join(s)
        # the ▁ character created bugs in the cpp tokenizer
        s = s.replace('ENDCOM', '\n').replace('▁', ' SPACETOKEN ')
        try:
            tokens_and_types = get_cpp_tokens_and_types(s)
        except:
            return ''
        new_tokens = []
        i = 0
        while i < len(tokens_and_types):
            token, type = tokens_and_types[i]
            if type in STRINGS_AND_COMMENTS_TOKEN_KINDS:
                new_tokens.append(token.replace('STRNEWLINE', '\n').replace(
                    'TABSYMBOL', '\t').replace(' ', '').replace('SPACETOKEN', ' '))
                if type == Tokeniser.TokenKind.COMMENT:
                    new_tokens.append('NEW_LINE')
            elif token == '}':
                if i < len(tokens_and_types) - 1 and tokens_and_types[i + 1][0] == ';':
                    new_tokens += ['CB_COLON', 'NEW_LINE']
                    i += 2
                    continue
                if i < len(tokens_and_types) - 1 and tokens_and_types[i + 1][0] == ',':
                    new_tokens += ['CB_COMA', 'NEW_LINE']
                    i += 2
                    continue
                new_tokens += ['CB_', 'NEW_LINE']
            elif token == '{':
                new_tokens += ['OB_', 'NEW_LINE']
            elif token == '*/':
                new_tokens += ['*/', 'NEW_LINE']
            elif token == ';':
                new_tokens += [';', 'NEW_LINE']
            else:
                new_tokens.append(token)

            if i < len(tokens_and_types) - 1 and tokens_and_types[i + 1][0] in TOK_NO_SPACE_BEFORE:
                next_token = tokens_and_types[i + 1][0]
                new_tokens[len(new_tokens) - 1] += next_token
                if next_token == ';':
                    new_tokens.append('NEW_LINE')
                i += 2
                continue
            i += 1

        lines = re.split('NEW_LINE', ' '.join(new_tokens))

        untok_s = indent_lines(lines)
        untok_s = untok_s.replace('CB_COLON', '};').replace(
            'CB_COMA', '},').replace('CB_', '}').replace('OB_', '{')
        return untok_s

with open('Testing.cs', 'r') as fin:
    tokens = fin.read()
    h=tokenize_cpp(tokens)
    fout = open("output_cpp_testing.txt", "w",encoding='utf-8')
    for token in h:
        fout.write(token + "\n")